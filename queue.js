let collection = [];

// ===== Number 1 ===== //
function print() {
  return collection;
}



// ===== Number 2, 3, 5 and 6===== //
function enqueue(element) {
	collection.push(element)
	return collection;
}



// ===== Number 4 ===== //
function dequeue() {
	collection.shift()
	return collection;
}



// ===== Number 7 ===== //
function front() {
	return collection[0];
}



// ===== Number 8 ===== //
function size() {
	return collection.length;
}



// ===== Number 9 ===== //
function isEmpty() {
	return collection === "";
}





module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};

